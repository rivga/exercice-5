<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/hello', function(){
    return 'Hello Laravel';
});

Route::get('/student/{id}', function($id='No student'){
    return 'We got student with id '.$id;
});

Route::get('/car/{id}', function($id = null){
    if (isset($id)){
        return "We got car $id";
    } else {
        return 'We need id to find ur car';
    }
});

Route::get('/comment/{id}', function ($id) {
    return view('comment',compact('id'));
});


//ex5
Route::get('/users/{email}/{name?}', function($email, $name = null){ 
    if (!isset($name)){
        $name='Missing name';
    }
    return view('users', compact('email','name'));
});


Route::resource('candidates','CandidatesController')->middleware('auth');
Route::get('candidates/changestatus/{cid}/{sid}', 'CandidatesController@changeStatus')->name('candidate.changestatus');

Route::get('mycandidates', 'CandidatesController@myCandidates')->name('candidates.mycandidates')->middleware('auth');

Route::get('candidates/changeUser/{cid}/{uid?}', 'CandidatesController@changeUser')->name('candidate.changeuser');
Auth::routes();
route::get('candidate/delete/{id}','CandidatesController@destroy')->name('candidate.delete');

Route::get('/home', 'HomeController@index')->name('home');

